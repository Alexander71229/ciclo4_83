const mgdb=require('../BaseDeDatos/db.js');
const nombreModelo="roles";
mgdb.model(nombreModelo,{
	"nombre":{type:String,require:true,unique:true},
	"permisos":[{type:String}]
});
module.exports=mgdb.model(nombreModelo);