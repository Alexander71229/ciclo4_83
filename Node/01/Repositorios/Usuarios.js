const { Schema } = require('mongoose');
const mgdb=require('../BaseDeDatos/db.js');
const nombreModelo="usuarios";
mgdb.model(nombreModelo,{
	"login":{type:String,require:true,unique:true},
	"clave":String,
	"roles":[{ref:"roles",type:Schema.Types.ObjectId}]
});
module.exports=mgdb.model(nombreModelo);