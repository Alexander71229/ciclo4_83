const mgdb=require('../BaseDeDatos/db.js');
const nombreModelo="productos";
mgdb.model(nombreModelo,{
	"id":{type:Number,require:true,unique:true},
	"nombre":String,
	"precio":Number
});
module.exports=mgdb.model(nombreModelo);