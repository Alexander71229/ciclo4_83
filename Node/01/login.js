const express=require('express');
const cifrador=require('bcrypt');
const jwt=require('jsonwebtoken');
const repositorio=require('./Repositorios/Usuarios.js');
const config=require('./Config.js');
const rutas=express.Router();
rutas.route("/").post((req,res)=>{
	repositorio.findOne({login:req.body.login}).then(datos=>{
		if(!datos){
			res.send({r:0,m:"No existe el usuario"});
		}
		if(cifrador.compareSync(req.body.clave,datos.clave)){
			datos.clave="";
			const token=jwt.sign({login:datos.login,roles:datos.roles},config.getClave());
			res.send({token});
		}else{console.trace('');
			res.sendStatus(403);
		}
	}).catch((e)=>{
		console.log(e);
		res.sendStatus(403);
	});
});
rutas.route("/signup").post((req,res)=>{
	try{
		console.log(req.body);
		const hash=cifrador.hashSync(req.body.clave,12);
		req.body.clave=hash;
		repositorio.create(req.body).then(()=>{
			res.send({r:0});
		}).catch((e)=>{
			console.log(e);
			res.send({r:1});
		});
	}catch(e){
		console.log(e);
		res.send({r:1,e:e});
	}
});
const validarToken=(req,res)=>{
	try{
		jwt.verify(req.headers.authorization.split(" ")[1],config.getClave());
		return true;
	}catch(e){
	}
};
module.exports=rutas;
module.exports.validarToken=validarToken;