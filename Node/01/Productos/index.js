const express=require('express');
const rutas=express.Router();
const repositorio=require('../Repositorios/Producto.js');
rutas.route("/").get((req,res)=>{
	repositorio.find({}).then(datos=>res.send(datos)).catch(e=>res.send({respuesta:1,error:e}));
});
rutas.route("/:id").get((req,res)=>{
	repositorio.findOne({id:req.params.id}).then(datos=>res.send(datos)).catch(e=>res.send({respuesta:1,error:e}));
});
rutas.route("/crear").post((req,res)=>{
	repositorio.create(req.body).then(()=>res.send({respuesta:0})).catch(e=>res.send({respuesta:1,error:e}));
});
rutas.route("/actualizar").put((req,res)=>{
	repositorio.findByIdAndUpdate(req.body._id,req.body).then(()=>res.send({respuesta:0})).catch(e=>res.send({respuesta:1,error:e}));
});
rutas.route("/eliminar").delete((req,res)=>{
	repositorio.findByIdAndDelete(req.body._id).then(()=>res.send({respuesta:0})).catch(e=>res.send({respuesta:1,error:e}));
});
module.exports=rutas;