const express=require('express');
const cors=require('cors');
const app=express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
const login=require('./login.js');
app.use("/login",login);
app.use((req,res,next)=>{
	if(login.validarToken(req,res)){
		next();
	}else{
		res.sendStatus(401);
	}
})
const rutasProducto=require('./Productos/index.js');
app.use("/productos",rutasProducto);
app.listen(2347,()=>{
	console.log('Servidor iniciado');
});