import { useState } from "react";
import axios from "axios";
import qs from 'qs';
export default function Login(){
	const[login,setLogin]=useState("");
	const[clave,setClave]=useState("");
	const aceptar=(evento)=>{
		evento.preventDefault();
		//axios.post("https://any-api.com:8443/https://api.personio.de/v1/auth",{login,clave})//{login:login,clave:clave}
		console.log(qs.stringify({login,clave}));
		axios.post("https://any-api.com:8443/https://api.personio.de/v1/auth",qs.stringify({login,clave}))
		.then(({data})=>alert(data))
		.catch(console.error); 
	}
	return(
		<form onSubmit={aceptar}>
			<div>
				login:<input onChange={e=>setLogin(e.target.value)}/>
			</div>
			<div>
				clave:<input type="password" onChange={e=>setClave(e.target.value)}/>
			</div>
			<div>
				<button>Aceptar</button>
			</div>
		</form>
	);
}