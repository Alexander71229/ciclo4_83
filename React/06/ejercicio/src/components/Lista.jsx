import { useEffect, useState } from "react";
import axios from "axios";
import Pokemon from "./Pokemon.jsx";
export default function Lista(){
	const[datos,setDatos]=useState([]);//const arreglo=useState([]);const datos=arreglo[0];const setDatos=arreglo[1];
	useEffect(()=>{
		axios.get("https://pokeapi.co/api/v2/pokemon")
		.then(({data})=>setDatos(data.results))
		.catch(console.error);
	},[]);
	return(
		<div>
			{datos.map((pokemon,indice)=><Pokemon key={indice} datos={pokemon}/>)}
		</div>
	);
}