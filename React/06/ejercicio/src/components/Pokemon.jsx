import { useEffect, useState } from "react";
import axios from "axios";
import { Suspense } from "react";
export default function Pokemon({datos}){//props={datos:{name:"ditto",url:"ruta de la información"}}
	const[informacion,setInformacion]=useState({});
	useEffect(()=>{
		axios.get(datos.url)
		.then(({data})=>setInformacion(data))
		.catch(console.error);
	},[informacion,datos.url]);
	return(
		<fieldset>
			<legend>{datos.name}</legend>
			<div>
				<Suspense fallback={<h1>Cargando</h1>}>
					{informacion&&informacion.sprites?<img src={informacion.sprites.front_default} alt=""/>:"..."}
				</Suspense>
			</div>
		</fieldset>
	);
}