import { BrowserRouter,Routes,Route } from "react-router-dom";
import Home from "./Home.jsx";
import Lista from "./Lista.jsx";
import Login from "./Login.jsx";
export default function Contenedor(){
	return(
		<BrowserRouter>
			<Routes>
				<Route path="/" element={<Home/>}/>
				<Route path="/pokemones" element={<Lista/>}/>
				<Route path="/login" element={<Login/>}/>
			</Routes>
		</BrowserRouter>
	);
}