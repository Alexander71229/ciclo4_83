import React,{useState} from "react";
import Elemento from "./Elemento";
export default function Lista(){
	const[lista,actualizarLista]=useState(['Valor 1','Valor 2']);
	const incrementar=()=>{
		actualizarLista([...lista,'Valor '+(lista.length+1)]);
	}
	const decrementar=()=>{
		actualizarLista([...lista].filter((e,i)=>i<lista.length-1));
	}
	const eliminar=(id)=>{
		console.log(id);
		let copiaArreglo=[...lista];
		copiaArreglo.splice(id,1);
		console.log(copiaArreglo);
		actualizarLista(copiaArreglo);
	}
	return(
		<>
			<button onClick={incrementar}>Incrementar</button>
			<button onClick={decrementar}>Decrementar</button>
			<div>
				{lista.map((elemento,indice)=><Elemento key={indice} i={indice} f={eliminar}>{elemento}</Elemento>)}
			</div>
		</>
	);
}