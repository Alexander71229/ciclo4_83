import React from "react";
export default function Contador({cuentaInicial,incremento,children}){
	console.log(children);
	const [cuenta,actualizarCuenta]=React.useState(cuentaInicial);
	const incrementar=()=>actualizarCuenta(cuenta+incremento);
	return(<>
		<div>Valor del contador:{cuenta}</div>
		<button onClick={incrementar}>Aumentar</button>
		<button onClick={()=>{actualizarCuenta(cuenta-incremento)}}>Disminuir</button>
		{children}
		</>
	);
}