import { useState } from "react";
import axios from "axios";
export default function Login(){
	const[login,setLogin]=useState("");
	const[clave,setClave]=useState("");
	const aceptar=(e)=>{
		sessionStorage.setItem('token',null)
		e.preventDefault();
		axios.post('http://127.0.0.1:2347/login/',{login,clave})
		.catch(console.log)
		.then(({data})=>{
			if(data.token){
				return sessionStorage.setItem('token',data.token);
			}
			alert(data.m);
		});
	}
	return(
		<form onSubmit={aceptar}>
			<div>
				Login:
				<input value={login} onChange={evento=>setLogin(evento.target.value)}></input>
			</div>
			<div>
				Clave:
				<input type='password' value={clave} onChange={e=>setClave(e.target.value)}></input>
			</div>
			<div>
				<button>Aceptar</button>
			</div>
		</form>
	);
}