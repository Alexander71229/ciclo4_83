import React from "react";
export default function useFetch(url,token){
	const[datos,setDatos]=React.useState([]);
	React.useEffect(()=>{
		console.log("window.token:"+token);
		fetch(url,{method:"GET",headers:{"Authorization":'Bearer '+token}}).catch(console.log)
			.then(r=>r.json())
			.then(d=>setDatos(d));
	},[url]);
	return[datos];
}