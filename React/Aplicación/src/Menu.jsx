import {BrowserRouter, Link, Routes, Route} from 'react-router-dom';
import Home from './Home.jsx';
import Formulario from './Formulario';
import ListaProductos from './ListaProductos.jsx';
import SignUp from './SignUp.jsx';
import Login from './Login.jsx';
export default function Menu(){
	return(
		<BrowserRouter>
			<ul>
				<li>
					<Link to="/">Página inicial</Link>
				</li>
				<li>
					<Link to="/producto/agregar">Agregar producto</Link>
				</li>
				<li>
					<Link to="/producto/">Listar productos</Link>
				</li>
				<li>
					<Link to="/signup/">Registrarse</Link>
				</li>
				<li>
					<Link to="/login/">Login</Link>
				</li>
			</ul>
			<Routes>
				<Route path="/" element={<Home/>}/>
				<Route path="/producto/agregar" element={<Formulario/>}/>
				<Route path="/producto/" element={<ListaProductos/>}/>
				<Route path="/signup/" element={<SignUp/>}/>
				<Route path="/login/" element={<Login/>}/>
			</Routes>
		</BrowserRouter>
	);
}