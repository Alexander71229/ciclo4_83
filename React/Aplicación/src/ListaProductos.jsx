import useFetch from "./useFetch";
export default function ListaProductos(){
	const[lista]=useFetch('http://127.0.0.1:2347/productos',sessionStorage.getItem('token'));
	return(
		<div>
			{lista.map((x,i)=><div key={i}>
				<span>{x.nombre+":"+x.precio}</span>
			</div>)}
		</div>
	);
}