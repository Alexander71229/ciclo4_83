import { useState } from "react";
import axios from "axios";
export default function SignUp(){
	const[login,setLogin]=useState("");
	const[clave,setClave]=useState("");
	const aceptar=(e)=>{
		e.preventDefault();
		axios.post('http://127.0.0.1:2347/login/signup',{login,clave}).then(({data})=>console.log(data));
	}
	return(
		<form onSubmit={aceptar}>
			<div>
				Login:
				<input value={login} onChange={evento=>setLogin(evento.target.value)}></input>
			</div>
			<div>
				Clave:
				<input type='password' value={clave} onChange={e=>setClave(e.target.value)}></input>
			</div>
			<div>
				<button>Aceptar</button>
			</div>
		</form>
	);
}