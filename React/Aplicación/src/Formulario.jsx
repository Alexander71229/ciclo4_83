import { useState } from "react";

export default function Formulario(){
	const[nombre,setNombre]=useState("");
	const[precio,setPrecio]=useState("");
	const aceptar=(e)=>{
	}
	return(
		<form onSubmit={aceptar}>
			<div>
				Nombre:
				<input value={nombre} onChange={evento=>setNombre(evento.target.value)}></input>
			</div>
			<div>
				Precio:
				<input value={precio} onChange={e=>setPrecio(e.target.value)}></input>
			</div>
			<div>
				<button>Aceptar</button>
			</div>
		</form>
	);
}