import {BrowserRouter, Link, Routes, Route} from 'react-router-dom';
import Home from './Home.jsx';
import Formulario from './Formulario';
export default function Menu(){
	return(
		<BrowserRouter>
			<ul>
				<li>
					<Link to="/">Página inicial</Link>
				</li>
				<li>
					<Link to="/formulario">Formulario</Link>
				</li>
			</ul>
			<Routes>
				<Route path="/" element={<Home/>}/>
				<Route path="/formulario" element={<Formulario/>}/>
			</Routes>
		</BrowserRouter>
	);
}