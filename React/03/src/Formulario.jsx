import React, { useEffect, useRef } from "react";
import './Formulario.css';
export default function Formulario(){
	const referencia=useRef(null);
	const manejarSubmit=(evento)=>{
		evento.preventDefault();
		console.log(referencia.current.value);
	}
	useEffect(()=>{
		console.log('Carga');
	});
	return(
		<form onSubmit={evento=>manejarSubmit(evento)}>
			<div style={{color:'red'}} className="estilo">
				usuario
				<input ref={referencia}></input>
			</div>
			<div>
				<input type='submit' value='OK'></input>
			</div>
		</form>
	);
}