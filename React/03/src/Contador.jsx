import React from "react";
export default function Contador(){
	const[cuenta,setCuenta]=React.useState(0);
	let contador=React.useRef(0);
	React.useEffect(()=>{
		contador.current=contador.current+1;
		console.log(contador.current);
		setTimeout(()=>{
			setCuenta(cuenta+1);
		},1000);
	});
	return(<div>{cuenta}</div>);
}