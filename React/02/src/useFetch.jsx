import React from "react";
export default function useFetch(url){
	const[datos,setDatos]=React.useState([]);
	React.useEffect(()=>{
		fetch(url)
			.then(r=>r.json())
			.then(d=>setDatos(d));
	},[url]);
	return[datos];
}