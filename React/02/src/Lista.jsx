import React from "react";
import useFetch from "./useFetch";
export default function Lista(){
	const[lista]=useFetch('https://jsonplaceholder.typicode.com/todos/');
	return(
		<div>
			<div>Lista obtenida desde el API</div>
			{lista.map(e=><div><span>{e.id}</span><span>{e.title}</span></div>)}
		</div>
	);
}